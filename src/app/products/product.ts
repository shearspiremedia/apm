export interface IProduct {
  productId: number;
  productName: string;
  productCode: string;
  releaseDate: string;
  price: number;
  description: string;
  starRating: number;
  imageUrl: string;
}
/*
In our interface file, we could define a class for the product business object.
This is only needed if we wish to have some additional functionality such as the
calculateDiscount() class method as seen here.
*/
export class Product implements IProduct {
  constructor(
    public productId: number,
    public productName: string,
    public productCode: string,
    public releaseDate: string,
    public price: number,
    public description: string,
    public starRating: number,
    public imageUrl: string
  ){}

  calculateDiscount(percent: number): number {
    return this.price = (this.price * percent / 100);
  }
}
